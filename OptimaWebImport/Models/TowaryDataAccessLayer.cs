﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace OptimaWebImport.Models
{
    public class TowaryDataAccessLayer
    {
        CDN_FirmaTestContext _db = new CDN_FirmaTestContext();

        public void AddItem(BOMModel bom)
        {

            if (IfItemExists(bom))
            {
                return;
            }

            var newTowar = new Towary();
            List<TwrCeny> listaCeny = new List<TwrCeny>();

            newTowar.TwrNazwa = bom.Name;
            newTowar.TwrKod = bom.Name;
            newTowar.TwrStawka = 23.00m;
            newTowar.TwrFlaga = 2;
            newTowar.TwrTyp = 1; // 0 -usługa; 1 - towar 
            if (bom.Complex)
            {
                newTowar.TwrProdukt = 1; //1 - złożony; 0 - prosty
            }
            else
            {
                newTowar.TwrProdukt = 0;
            }

            newTowar.TwrTsZal = DateTime.Now;
            newTowar.TwrTsMod = DateTime.Now;
            newTowar.TwrJm = bom.Unit;
            newTowar.TwrCenaZczteremaMiejscami = 0;
            newTowar.TwrUdostepniajWcenniku = 1;
            newTowar.TwrOpis = bom.Description;


            newTowar.TwrAutoKodSeria = "";
            newTowar.TwrAutoKodWartosc = "";
            newTowar.TwrPlu = "";
            newTowar.TwrNumerKat = "";
            newTowar.TwrSww = "";
            newTowar.TwrEan = "";
            newTowar.TwrNazwaFiskalna = "";
            newTowar.TwrUrl = "";
            newTowar.TwrKategoria = "";
            newTowar.TwrKategoriaZak = "";
            
            newTowar.TwrJmz = "";
            newTowar.TwrWaluta = "";
            newTowar.TwrKodDostawcy = "";
            newTowar.TwrProducentKod = "";
            newTowar.TwrIloscMinJm = "";
            newTowar.TwrIloscMaxJm = "";
            newTowar.TwrIloscZamJm = "";
            newTowar.TwrKrajPochodzenia = "";
            newTowar.TwrOpeModKod = "";
            newTowar.TwrOpeModNazwisko = "";
            newTowar.TwrOpeZalKod = "";
            newTowar.TwrOpeZalNazwisko = "";
            newTowar.TwRWalutaZakOld = "";




            _db.Towary.Add(newTowar);
            
                _db.SaveChanges();
            
           

            for (int i = 1; i < 6; i++)
            {
                var newCena = new TwrCeny();
                newCena.TwCTwrId = newTowar.TwrTwrId;
                newCena.TwCWaluta = "PLN";
                newCena.TwCTwCnumer = i;
                if (i == 5)
                {
                    newCena.TwCTyp = 2;
                }
                else
                {
                    newCena.TwCTyp = 1;
                }
                newCena.TwCTyp = 1;
                listaCeny.Add(newCena);

            }
            _db.TwrCeny.AddRange(listaCeny);

           
                _db.SaveChanges();
            
            if (bom.Complex == true)
            {
                foreach (var child in bom.Children)
                {
                    if (!IfItemExists(child))
                    {
                        AddItem(child);
                    }



                }
                CreateRecipe(newTowar.TwrKod, bom.Children);
            }


        }

        public void CreateRecipe(string parentCode, List<BOMModel> children)
        {
            var newRecipe = new ProdReceptury();
            var query = _db.Towary;

            newRecipe.PdRTwrId = query.Where(x => x.TwrKod == parentCode).Single().TwrTwrId;
            newRecipe.PdRKod = $"{parentCode}_Rec";
            newRecipe.PdRTsZal = DateTime.Now;
            newRecipe.PdRTsMod = DateTime.Now;

            newRecipe.PdRNazwa = $"Receptura: {query.Where(x => x.TwrKod == parentCode).Single().TwrKod}";
            newRecipe.PdRJm = query.Where(x => x.TwrKod == parentCode).Single().TwrJm;
            newRecipe.PdROpis = "";
            newRecipe.PdROpeModKod = "TU";
            newRecipe.PdROpeModNazwisko = "Test User";
            newRecipe.PdROpeZalKod = "TU";
            newRecipe.PdROpeZalNazwisko = "Test User";
            _db.ProdReceptury.Add(newRecipe);

           
                _db.SaveChanges();
            
           


            var IngrList = new List<ProdSkladniki>();
            foreach (var child in children)
            {
                //if (!IfItemExists(child))
                //{
                //    AddItem(child);
                //}
                var newRecipeIngr = new ProdSkladniki();

                newRecipeIngr.PdSPdRid = newRecipe.PdRPdRid;
                newRecipeIngr.PdSProdId = newRecipe.PdRTwrId.GetValueOrDefault();

                newRecipeIngr.PdSTwrId = query.Where(x => x.TwrKod == child.Name).Single().TwrTwrId;
                newRecipeIngr.PdSJm = child.Unit;
                IngrList.Add(newRecipeIngr);
            }
            _db.ProdSkladniki.AddRange(IngrList);

          
                _db.SaveChanges();
            
            


        }
        public bool IfItemExists(BOMModel bom)
        {
            var query = _db.Towary.Where(x => x.TwrKod == bom.Name).FirstOrDefault();

            if (query != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void ListItems()
        {

            var query = _db.Towary;

            foreach (var item in query)
            {
                Console.WriteLine(item.TwrKod);
            }

        }

    }
}
