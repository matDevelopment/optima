﻿using System;
using System.Collections.Generic;

namespace OptimaWebImport.Models
{
    public partial class ProdReceptury
    {
        public ProdReceptury()
        {
            ProdSkladniki = new HashSet<ProdSkladniki>();
        }

        public int PdRPdRid { get; set; }
        public int? PdRTwrId { get; set; }
        public string PdRKod { get; set; }
        public string PdRNazwa { get; set; }
        public byte PdRDomyslna { get; set; }
        public decimal PdRIlosc { get; set; }
        public string PdRJm { get; set; }
        public string PdROpis { get; set; }
        public int? PdROpeZalId { get; set; }
        public int? PdRStaZalId { get; set; }
        public DateTime PdRTsZal { get; set; }
        public int? PdROpeModId { get; set; }
        public int? PdRStaModId { get; set; }
        public DateTime PdRTsMod { get; set; }
        public string PdROpeModKod { get; set; }
        public string PdROpeModNazwisko { get; set; }
        public string PdROpeZalKod { get; set; }
        public string PdROpeZalNazwisko { get; set; }

        public Towary PdRTwr { get; set; }
        public ICollection<ProdSkladniki> ProdSkladniki { get; set; }
    }
}
