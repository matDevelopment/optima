﻿using System;
using System.Collections.Generic;

namespace OptimaWebImport.Models
{
    public partial class ProdSkladniki
    {
        public int PdSPdSid { get; set; }
        public int PdSPdRid { get; set; }
        public int PdSProdId { get; set; }
        public int? PdSTwrId { get; set; }
        public int? PdSMagId { get; set; }
        public decimal PdSIlosc { get; set; }
        public string PdSJm { get; set; }
        public byte PdSJmcalkowite { get; set; }
        public decimal PdSJmprzelicznikL { get; set; }
        public decimal PdSJmprzelicznikM { get; set; }
        public decimal PdSIloscJm { get; set; }

        public ProdReceptury PdSPdR { get; set; }
        public Towary PdSTwr { get; set; }
    }
}
