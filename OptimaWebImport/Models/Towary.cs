﻿using System;
using System.Collections.Generic;

namespace OptimaWebImport.Models
{
    public partial class Towary
    {
        public Towary()
        {
            ProdReceptury = new HashSet<ProdReceptury>();
            ProdSkladniki = new HashSet<ProdSkladniki>();
            TwrCeny = new HashSet<TwrCeny>();
        }

        public int TwrTwrId { get; set; }
        public byte TwrTyp { get; set; }
        public short TwrProdukt { get; set; }
        public string TwrKod { get; set; }
        public byte TwrAutoKodGrupaTowarowa { get; set; }
        public string TwrAutoKodSeria { get; set; }
        public string TwrAutoKodWartosc { get; set; }
        public int TwrAutoKodNumer { get; set; }
        public string TwrPlu { get; set; }
        public string TwrNumerKat { get; set; }
        public string TwrSww { get; set; }
        public string TwrEan { get; set; }
        public string TwrNazwa { get; set; }
        public string TwrNazwaFiskalna { get; set; }
        public int TwrTwGgidnumer { get; set; }
        public string TwrUrl { get; set; }
        public int? TwrKatId { get; set; }
        public string TwrKategoria { get; set; }
        public int? TwrKatZakId { get; set; }
        public string TwrKategoriaZak { get; set; }
        public string TwrOpis { get; set; }
        public byte TwrEdycjaNazwy { get; set; }
        public byte TwrKopiujOpis { get; set; }
        public byte TwrEdycjaOpisu { get; set; }
        public string TwrJm { get; set; }
        public byte TwrJmcalkowite { get; set; }
        public string TwrJmz { get; set; }
        public decimal TwrJmprzelicznikL { get; set; }
        public decimal TwrJmprzelicznikM { get; set; }
        public byte TwrKaucja { get; set; }
        public int TwrTwCnumer { get; set; }
        public byte TwrUdostepniajWcenniku { get; set; }
        public string TwrWaluta { get; set; }
        public int TwrKursNumer { get; set; }
        public decimal TwrKursL { get; set; }
        public decimal TwrKursM { get; set; }
        public decimal TwrMinCenaMarza { get; set; }
        public byte TwrTypMinimum { get; set; }
        public decimal TwrKosztUslugiWal { get; set; }
        public decimal TwrKosztUslugi { get; set; }
        public byte TwrKosztUslugiTyp { get; set; }
        public decimal TwrKosztKgo { get; set; }
        public int? TwrSonid { get; set; }
        public int? TwrKntId { get; set; }
        public string TwrKodDostawcy { get; set; }
        public string TwrProducentKod { get; set; }
        public decimal TwrIloscMin { get; set; }
        public string TwrIloscMinJm { get; set; }
        public decimal TwrIloscMax { get; set; }
        public string TwrIloscMaxJm { get; set; }
        public decimal TwrIloscZam { get; set; }
        public string TwrIloscZamJm { get; set; }
        public decimal TwrStawka { get; set; }
        public short TwrFlaga { get; set; }
        public decimal TwrZrodlowa { get; set; }
        public decimal TwrStawkaExport { get; set; }
        public short TwrFlagaExport { get; set; }
        public decimal TwrZrodlowaExport { get; set; }
        public decimal TwrStawkaZak { get; set; }
        public short TwrFlagaZak { get; set; }
        public decimal TwrZrodlowaZak { get; set; }
        public byte TwrProg { get; set; }
        public byte TwrUpust { get; set; }
        public byte TwrUpustData { get; set; }
        public int? TwrUpustDataOd { get; set; }
        public int? TwrUpustDataDo { get; set; }
        public byte TwrUpustGodz { get; set; }
        public int? TwrUpustGodzOd { get; set; }
        public int? TwrUpustGodzDo { get; set; }
        public byte TwrBezRabatu { get; set; }
        public decimal TwrMarzaMin { get; set; }
        public byte TwrNieAktywny { get; set; }
        public byte? TwrEsklep { get; set; }
        public int? TwrKcnid { get; set; }
        public decimal TwrMasa { get; set; }
        public byte TwrSent { get; set; }
        public decimal TwrJmsentPrzelicznikL { get; set; }
        public decimal TwrJmsentPrzelicznikM { get; set; }
        public decimal TwrSentMasaBrutto { get; set; }
        public int? TwrPrdId { get; set; }
        public int? TwrMrkId { get; set; }
        public int? TwrIgaleriaKatId { get; set; }
        public short TwrEsklepBezRabatu { get; set; }
        public short TwrEsklepStatus { get; set; }
        public short TwrEsklepDostepnosc { get; set; }
        public byte TwrEsklepKalkulacjaDostaw { get; set; }
        public decimal TwrEsklepKalkulacjaDostawWartosc { get; set; }
        public byte TwrEsklepFlagaNowosc { get; set; }
        public byte TwrEsklepFlagaPromocja { get; set; }
        public byte TwrEsklepFlagaProduktzGazetki { get; set; }
        public byte TwrEsklepFlagaProduktPolecany { get; set; }
        public byte TwrEsklepFlagaWyprzedaz { get; set; }
        public byte TwrEsklepFlagaSuperJakosc { get; set; }
        public byte TwrEsklepFlagaSuperCena { get; set; }
        public byte TwrEsklepFlagaNajlepiejOceniany { get; set; }
        public byte TwrEsklepFlagaRekomendacjaSprzedawcy { get; set; }
        public int? TwrEsklepFantomId { get; set; }
        public decimal? TwrWagaKg { get; set; }
        public string TwrKrajPochodzenia { get; set; }
        public decimal TwrJmPomPrzelicznikL { get; set; }
        public decimal TwrJmPomPrzelicznikM { get; set; }
        public int? TwrOpeZalId { get; set; }
        public int? TwrStaZalId { get; set; }
        public DateTime TwrTsZal { get; set; }
        public int? TwrOpeModId { get; set; }
        public int? TwrStaModId { get; set; }
        public DateTime? TwrTsXl { get; set; }
        public DateTime TwrTsMod { get; set; }
        public string TwrOpeModKod { get; set; }
        public string TwrOpeModNazwisko { get; set; }
        public string TwrOpeZalKod { get; set; }
        public string TwrOpeZalNazwisko { get; set; }
        public short? TwrGidtyp { get; set; }
        public int? TwrGidfirma { get; set; }
        public int? TwrGidnumer { get; set; }
        public short? TwrGidlp { get; set; }
        public byte TwrAkcyza { get; set; }
        public decimal TwrAkcyzaJmpomPrzelicznikL { get; set; }
        public int TwrAkcyzaJmpomPrzelicznikM { get; set; }
        public decimal TwrAkcyzaOpal { get; set; }
        public decimal TwrAkcyzaStawka { get; set; }
        public byte TwrOdwrotneObciazenie { get; set; }
        public byte TwrMobile { get; set; }
        public byte TwrPobieranieSkladnikowFsuslugaZlozona { get; set; }
        public decimal TwRKosztUslugiOld { get; set; }
        public string TwRWalutaZakOld { get; set; }
        public decimal TwRKursLzakOld { get; set; }
        public decimal TwRKursMzakOld { get; set; }
        public byte TwrCenaZczteremaMiejscami { get; set; }

        public ICollection<ProdReceptury> ProdReceptury { get; set; }
        public ICollection<ProdSkladniki> ProdSkladniki { get; set; }
        public ICollection<TwrCeny> TwrCeny { get; set; }
    }
}
