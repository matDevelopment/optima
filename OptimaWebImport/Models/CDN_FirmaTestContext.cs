﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace OptimaWebImport.Models
{
    public partial class CDN_FirmaTestContext : DbContext
    {
        public CDN_FirmaTestContext()
        {
        }

        public CDN_FirmaTestContext(DbContextOptions<CDN_FirmaTestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProdReceptury> ProdReceptury { get; set; }
        public virtual DbSet<ProdSkladniki> ProdSkladniki { get; set; }
        public virtual DbSet<Towary> Towary { get; set; }
        public virtual DbSet<TwrCeny> TwrCeny { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("data source=ADNMARCIN\\OPTIMA;initial catalog=CDN_FirmaTest;integrated security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdReceptury>(entity =>
            {
                entity.HasKey(e => e.PdRPdRid);

                entity.ToTable("ProdReceptury", "CDN");

                entity.HasIndex(e => new { e.PdRTwrId, e.PdRKod })
                    .HasName("PdRTwrKod")
                    .IsUnique();

                entity.Property(e => e.PdRPdRid).HasColumnName("PdR_PdRId");

                entity.Property(e => e.PdRDomyslna).HasColumnName("PdR_Domyslna");

                entity.Property(e => e.PdRIlosc)
                    .HasColumnName("PdR_Ilosc")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.PdRJm)
                    .IsRequired()
                    .HasColumnName("PdR_JM")
                    .HasMaxLength(20);

                entity.Property(e => e.PdRKod)
                    .IsRequired()
                    .HasColumnName("PdR_Kod")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PdRNazwa)
                    .IsRequired()
                    .HasColumnName("PdR_Nazwa")
                    .HasMaxLength(50);

                entity.Property(e => e.PdROpeModId).HasColumnName("PdR_OpeModID");

                entity.Property(e => e.PdROpeModKod)
                    .IsRequired()
                    .HasColumnName("PdR_OpeModKod")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PdROpeModNazwisko)
                    .IsRequired()
                    .HasColumnName("PdR_OpeModNazwisko")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PdROpeZalId).HasColumnName("PdR_OpeZalID");

                entity.Property(e => e.PdROpeZalKod)
                    .IsRequired()
                    .HasColumnName("PdR_OpeZalKod")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PdROpeZalNazwisko)
                    .IsRequired()
                    .HasColumnName("PdR_OpeZalNazwisko")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PdROpis)
                    .IsRequired()
                    .HasColumnName("PdR_Opis")
                    .HasMaxLength(254);

                entity.Property(e => e.PdRStaModId).HasColumnName("PdR_StaModId");

                entity.Property(e => e.PdRStaZalId).HasColumnName("PdR_StaZalId");

                entity.Property(e => e.PdRTsMod)
                    .HasColumnName("PdR_TS_Mod")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdRTsZal)
                    .HasColumnName("PdR_TS_Zal")
                    .HasColumnType("datetime");

                entity.Property(e => e.PdRTwrId).HasColumnName("PdR_TwrId");

                entity.HasOne(d => d.PdRTwr)
                    .WithMany(p => p.ProdReceptury)
                    .HasForeignKey(d => d.PdRTwrId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PdRTwrKod");
            });

            modelBuilder.Entity<ProdSkladniki>(entity =>
            {
                entity.HasKey(e => e.PdSPdSid);

                entity.ToTable("ProdSkladniki", "CDN");

                entity.HasIndex(e => e.PdSMagId)
                    .HasName("PdSMagazyn");

                entity.HasIndex(e => e.PdSProdId)
                    .HasName("PdSProdukt");

                entity.HasIndex(e => e.PdSTwrId)
                    .HasName("PdSTowar");

                entity.HasIndex(e => new { e.PdSPdRid, e.PdSPdSid })
                    .HasName("PdSReceptura")
                    .IsUnique();

                entity.Property(e => e.PdSPdSid).HasColumnName("PdS_PdSId");

                entity.Property(e => e.PdSIlosc)
                    .HasColumnName("PdS_Ilosc")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.PdSIloscJm)
                    .HasColumnName("PdS_IloscJM")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.PdSJm)
                    .IsRequired()
                    .HasColumnName("PdS_Jm")
                    .HasMaxLength(20);

                entity.Property(e => e.PdSJmcalkowite).HasColumnName("PdS_JMCalkowite");

                entity.Property(e => e.PdSJmprzelicznikL)
                    .HasColumnName("PdS_JMPrzelicznikL")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.PdSJmprzelicznikM)
                    .HasColumnName("PdS_JMPrzelicznikM")
                    .HasColumnType("decimal(7, 0)");

                entity.Property(e => e.PdSMagId).HasColumnName("PdS_MagId");

                entity.Property(e => e.PdSPdRid).HasColumnName("PdS_PdRId");

                entity.Property(e => e.PdSProdId).HasColumnName("PdS_ProdId");

                entity.Property(e => e.PdSTwrId).HasColumnName("PdS_TwrId");

                entity.HasOne(d => d.PdSPdR)
                    .WithMany(p => p.ProdSkladniki)
                    .HasForeignKey(d => d.PdSPdRid)
                    .HasConstraintName("FK_PdSReceptura");

                entity.HasOne(d => d.PdSTwr)
                    .WithMany(p => p.ProdSkladniki)
                    .HasForeignKey(d => d.PdSTwrId)
                    .HasConstraintName("FK_PdSTowar");
            });

            modelBuilder.Entity<Towary>(entity =>
            {
                entity.HasKey(e => e.TwrTwrId);

                entity.ToTable("Towary", "CDN");

                entity.HasIndex(e => e.TwrIgaleriaKatId)
                    .HasName("TwrIGaleriaKategoria");

                entity.HasIndex(e => e.TwrKatId)
                    .HasName("Twrkategoria");

                entity.HasIndex(e => e.TwrKatZakId)
                    .HasName("TwrKategoriaZak");

                entity.HasIndex(e => e.TwrKcnid)
                    .HasName("TwrKodCN");

                entity.HasIndex(e => e.TwrKntId)
                    .HasName("TwrDostawca");

                entity.HasIndex(e => e.TwrKod)
                    .HasName("TwrKod")
                    .IsUnique();

                entity.HasIndex(e => e.TwrMrkId)
                    .HasName("TwrMarka");

                entity.HasIndex(e => e.TwrPrdId)
                    .HasName("TwrProducent");

                entity.HasIndex(e => e.TwrSonid)
                    .HasName("TwrSON");

                entity.HasIndex(e => e.TwrTwGgidnumer)
                    .HasName("TwrGrupaDomyslna");

                entity.HasIndex(e => new { e.TwrAutoKodSeria, e.TwrAutoKodNumer })
                    .HasName("TwrAutoKodNr");

                entity.HasIndex(e => new { e.TwrEan, e.TwrTwrId })
                    .HasName("TwrEAN")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrGidtyp, e.TwrGidnumer })
                    .HasName("TwrGID");

                entity.HasIndex(e => new { e.TwrKodDostawcy, e.TwrTwrId })
                    .HasName("TwrKodDostawcy")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrNazwa, e.TwrTwrId })
                    .HasName("TwrNazwa")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrNumerKat, e.TwrTwrId })
                    .HasName("TwrNrKat")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrProdukt, e.TwrTyp })
                    .HasName("TwrProduktTyp");

                entity.HasIndex(e => new { e.TwrTwGgidnumer, e.TwrKod })
                    .HasName("TwrGrupKod")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrNieAktywny, e.TwrNazwa, e.TwrEan, e.TwrKod, e.TwrTwrId })
                    .HasName("TwrGridLookupFilter")
                    .IsUnique();

                entity.HasIndex(e => new { e.TwrTwrId, e.TwrKod, e.TwrTyp, e.TwrProdukt, e.TwrGidnumer })
                    .HasName("TwrGIDNumer");

                entity.HasIndex(e => new { e.TwrTwrId, e.TwrKod, e.TwrTyp, e.TwrProdukt, e.TwrGidnumer, e.TwrNieAktywny })
                    .HasName("TwrNieaktywne");

                entity.Property(e => e.TwrTwrId).HasColumnName("Twr_TwrId");

                entity.Property(e => e.TwRKosztUslugiOld)
                    .HasColumnName("TwR_KosztUslugiOld")
                    .HasColumnType("decimal(17, 4)");

                entity.Property(e => e.TwRKursLzakOld)
                    .HasColumnName("TwR_KursLZakOld")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.TwRKursMzakOld)
                    .HasColumnName("TwR_KursMZakOld")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.TwRWalutaZakOld)
                    .IsRequired()
                    .HasColumnName("TwR_WalutaZakOld")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrAkcyza).HasColumnName("Twr_Akcyza");

                entity.Property(e => e.TwrAkcyzaJmpomPrzelicznikL)
                    .HasColumnName("Twr_AkcyzaJMPomPrzelicznikL")
                    .HasColumnType("decimal(15, 2)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrAkcyzaJmpomPrzelicznikM)
                    .HasColumnName("Twr_AkcyzaJMPomPrzelicznikM")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrAkcyzaOpal)
                    .HasColumnName("Twr_AkcyzaOpal")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrAkcyzaStawka)
                    .HasColumnName("Twr_AkcyzaStawka")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrAutoKodGrupaTowarowa).HasColumnName("Twr_AutoKodGrupaTowarowa");

                entity.Property(e => e.TwrAutoKodNumer).HasColumnName("Twr_AutoKodNumer");

                entity.Property(e => e.TwrAutoKodSeria)
                    .IsRequired()
                    .HasColumnName("Twr_AutoKodSeria")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrAutoKodWartosc)
                    .IsRequired()
                    .HasColumnName("Twr_AutoKodWartosc")
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrBezRabatu).HasColumnName("Twr_BezRabatu");

                entity.Property(e => e.TwrCenaZczteremaMiejscami).HasColumnName("Twr_CenaZCzteremaMiejscami");

                entity.Property(e => e.TwrEan)
                    .IsRequired()
                    .HasColumnName("Twr_EAN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TwrEdycjaNazwy).HasColumnName("Twr_EdycjaNazwy");

                entity.Property(e => e.TwrEdycjaOpisu).HasColumnName("Twr_EdycjaOpisu");

                entity.Property(e => e.TwrEsklep).HasColumnName("Twr_ESklep");

                entity.Property(e => e.TwrEsklepBezRabatu).HasColumnName("Twr_ESklepBezRabatu");

                entity.Property(e => e.TwrEsklepDostepnosc).HasColumnName("Twr_ESklepDostepnosc");

                entity.Property(e => e.TwrEsklepFantomId).HasColumnName("Twr_ESklepFantomID");

                entity.Property(e => e.TwrEsklepFlagaNajlepiejOceniany).HasColumnName("Twr_ESklepFlaga_NajlepiejOceniany");

                entity.Property(e => e.TwrEsklepFlagaNowosc).HasColumnName("Twr_ESklepFlaga_Nowosc");

                entity.Property(e => e.TwrEsklepFlagaProduktPolecany).HasColumnName("Twr_ESklepFlaga_ProduktPolecany");

                entity.Property(e => e.TwrEsklepFlagaProduktzGazetki).HasColumnName("Twr_ESklepFlaga_ProduktzGazetki");

                entity.Property(e => e.TwrEsklepFlagaPromocja).HasColumnName("Twr_ESklepFlaga_Promocja");

                entity.Property(e => e.TwrEsklepFlagaRekomendacjaSprzedawcy).HasColumnName("Twr_ESklepFlaga_RekomendacjaSprzedawcy");

                entity.Property(e => e.TwrEsklepFlagaSuperCena).HasColumnName("Twr_ESklepFlaga_SuperCena");

                entity.Property(e => e.TwrEsklepFlagaSuperJakosc).HasColumnName("Twr_ESklepFlaga_SuperJakosc");

                entity.Property(e => e.TwrEsklepFlagaWyprzedaz).HasColumnName("Twr_ESklepFlaga_Wyprzedaz");

                entity.Property(e => e.TwrEsklepKalkulacjaDostaw).HasColumnName("Twr_ESklepKalkulacjaDostaw");

                entity.Property(e => e.TwrEsklepKalkulacjaDostawWartosc)
                    .HasColumnName("Twr_ESklepKalkulacjaDostawWartosc")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrEsklepStatus)
                    .HasColumnName("Twr_ESklepStatus")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrFlaga).HasColumnName("Twr_Flaga");

                entity.Property(e => e.TwrFlagaExport).HasColumnName("Twr_FlagaExport");

                entity.Property(e => e.TwrFlagaZak).HasColumnName("Twr_FlagaZak");

                entity.Property(e => e.TwrGidfirma).HasColumnName("Twr_GIDFirma");

                entity.Property(e => e.TwrGidlp).HasColumnName("Twr_GIDLp");

                entity.Property(e => e.TwrGidnumer).HasColumnName("Twr_GIDNumer");

                entity.Property(e => e.TwrGidtyp).HasColumnName("Twr_GIDTyp");

                entity.Property(e => e.TwrIgaleriaKatId).HasColumnName("Twr_IGaleriaKatId");

                entity.Property(e => e.TwrIloscMax)
                    .HasColumnName("Twr_IloscMax")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.TwrIloscMaxJm)
                    .IsRequired()
                    .HasColumnName("Twr_IloscMaxJM")
                    .HasMaxLength(20);

                entity.Property(e => e.TwrIloscMin)
                    .HasColumnName("Twr_IloscMin")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.TwrIloscMinJm)
                    .IsRequired()
                    .HasColumnName("Twr_IloscMinJM")
                    .HasMaxLength(20);

                entity.Property(e => e.TwrIloscZam)
                    .HasColumnName("Twr_IloscZam")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.TwrIloscZamJm)
                    .IsRequired()
                    .HasColumnName("Twr_IloscZamJM")
                    .HasMaxLength(20);

                entity.Property(e => e.TwrJm)
                    .IsRequired()
                    .HasColumnName("Twr_JM")
                    .HasMaxLength(20);

                entity.Property(e => e.TwrJmPomPrzelicznikL)
                    .HasColumnName("Twr_JmPomPrzelicznikL")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrJmPomPrzelicznikM)
                    .HasColumnName("Twr_JmPomPrzelicznikM")
                    .HasColumnType("decimal(7, 0)");

                entity.Property(e => e.TwrJmcalkowite).HasColumnName("Twr_JMCalkowite");

                entity.Property(e => e.TwrJmprzelicznikL)
                    .HasColumnName("Twr_JMPrzelicznikL")
                    .HasColumnType("decimal(15, 2)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrJmprzelicznikM)
                    .HasColumnName("Twr_JMPrzelicznikM")
                    .HasColumnType("decimal(7, 0)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrJmsentPrzelicznikL)
                    .HasColumnName("Twr_JMSentPrzelicznikL")
                    .HasColumnType("decimal(15, 2)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrJmsentPrzelicznikM)
                    .HasColumnName("Twr_JMSentPrzelicznikM")
                    .HasColumnType("decimal(7, 0)")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrJmz)
                    .IsRequired()
                    .HasColumnName("Twr_JMZ")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrKatId).HasColumnName("Twr_KatId");

                entity.Property(e => e.TwrKatZakId).HasColumnName("Twr_KatZakId");

                entity.Property(e => e.TwrKategoria)
                    .IsRequired()
                    .HasColumnName("Twr_Kategoria")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TwrKategoriaZak)
                    .IsRequired()
                    .HasColumnName("Twr_KategoriaZak")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TwrKaucja).HasColumnName("Twr_Kaucja");

                entity.Property(e => e.TwrKcnid).HasColumnName("Twr_KCNId");

                entity.Property(e => e.TwrKntId).HasColumnName("Twr_KntId");

                entity.Property(e => e.TwrKod)
                    .IsRequired()
                    .HasColumnName("Twr_Kod")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TwrKodDostawcy)
                    .IsRequired()
                    .HasColumnName("Twr_KodDostawcy")
                    .HasMaxLength(50);

                entity.Property(e => e.TwrKopiujOpis).HasColumnName("Twr_KopiujOpis");

                entity.Property(e => e.TwrKosztKgo)
                    .HasColumnName("Twr_KosztKGO")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrKosztUslugi)
                    .HasColumnName("Twr_KosztUslugi")
                    .HasColumnType("decimal(17, 4)");

                entity.Property(e => e.TwrKosztUslugiTyp).HasColumnName("Twr_KosztUslugiTyp");

                entity.Property(e => e.TwrKosztUslugiWal)
                    .HasColumnName("Twr_KosztUslugiWal")
                    .HasColumnType("decimal(17, 4)");

                entity.Property(e => e.TwrKrajPochodzenia)
                    .IsRequired()
                    .HasColumnName("Twr_KrajPochodzenia")
                    .HasMaxLength(2);

                entity.Property(e => e.TwrKursL)
                    .HasColumnName("Twr_KursL")
                    .HasColumnType("decimal(15, 4)");

                entity.Property(e => e.TwrKursM)
                    .HasColumnName("Twr_KursM")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.TwrKursNumer).HasColumnName("Twr_KursNumer");

                entity.Property(e => e.TwrMarzaMin)
                    .HasColumnName("Twr_MarzaMin")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrMasa)
                    .HasColumnName("Twr_Masa")
                    .HasColumnType("decimal(15, 3)");

                entity.Property(e => e.TwrMinCenaMarza)
                    .HasColumnName("Twr_MinCenaMarza")
                    .HasColumnType("decimal(15, 3)");

                entity.Property(e => e.TwrMobile)
                    .HasColumnName("Twr_Mobile")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.TwrMrkId).HasColumnName("Twr_MrkID");

                entity.Property(e => e.TwrNazwa)
                    .IsRequired()
                    .HasColumnName("Twr_Nazwa")
                    .HasMaxLength(255);

                entity.Property(e => e.TwrNazwaFiskalna)
                    .IsRequired()
                    .HasColumnName("Twr_NazwaFiskalna")
                    .HasMaxLength(40)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrNieAktywny).HasColumnName("Twr_NieAktywny");

                entity.Property(e => e.TwrNumerKat)
                    .IsRequired()
                    .HasColumnName("Twr_NumerKat")
                    .HasMaxLength(40);

                entity.Property(e => e.TwrOdwrotneObciazenie).HasColumnName("Twr_OdwrotneObciazenie");

                entity.Property(e => e.TwrOpeModId).HasColumnName("Twr_OpeModID");

                entity.Property(e => e.TwrOpeModKod)
                    .IsRequired()
                    .HasColumnName("Twr_OpeModKod")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrOpeModNazwisko)
                    .IsRequired()
                    .HasColumnName("Twr_OpeModNazwisko")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrOpeZalId).HasColumnName("Twr_OpeZalID");

                entity.Property(e => e.TwrOpeZalKod)
                    .IsRequired()
                    .HasColumnName("Twr_OpeZalKod")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrOpeZalNazwisko)
                    .IsRequired()
                    .HasColumnName("Twr_OpeZalNazwisko")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrOpis)
                    .IsRequired()
                    .HasColumnName("Twr_Opis");

                entity.Property(e => e.TwrPlu)
                    .IsRequired()
                    .HasColumnName("Twr_PLU")
                    .HasMaxLength(18)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrPobieranieSkladnikowFsuslugaZlozona).HasColumnName("Twr_PobieranieSkladnikowFSUslugaZlozona");

                entity.Property(e => e.TwrPrdId).HasColumnName("Twr_PrdID");

                entity.Property(e => e.TwrProducentKod)
                    .IsRequired()
                    .HasColumnName("Twr_ProducentKod")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TwrProdukt).HasColumnName("Twr_Produkt");

                entity.Property(e => e.TwrProg).HasColumnName("Twr_Prog");

                entity.Property(e => e.TwrSent).HasColumnName("Twr_Sent");

                entity.Property(e => e.TwrSentMasaBrutto)
                    .HasColumnName("Twr_SentMasaBrutto")
                    .HasColumnType("decimal(15, 2)");

                entity.Property(e => e.TwrSonid).HasColumnName("Twr_SONId");

                entity.Property(e => e.TwrStaModId).HasColumnName("Twr_StaModId");

                entity.Property(e => e.TwrStaZalId).HasColumnName("Twr_StaZalId");

                entity.Property(e => e.TwrStawka)
                    .HasColumnName("Twr_Stawka")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrStawkaExport)
                    .HasColumnName("Twr_StawkaExport")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrStawkaZak)
                    .HasColumnName("Twr_StawkaZak")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrSww)
                    .IsRequired()
                    .HasColumnName("Twr_SWW")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TwrTsMod)
                    .HasColumnName("Twr_TS_Mod")
                    .HasColumnType("datetime");

                entity.Property(e => e.TwrTsXl)
                    .HasColumnName("Twr_TS_XL")
                    .HasColumnType("datetime");

                entity.Property(e => e.TwrTsZal)
                    .HasColumnName("Twr_TS_Zal")
                    .HasColumnType("datetime");

                entity.Property(e => e.TwrTwCnumer).HasColumnName("Twr_TwCNumer");

                entity.Property(e => e.TwrTwGgidnumer).HasColumnName("Twr_TwGGIDNumer");

                entity.Property(e => e.TwrTyp).HasColumnName("Twr_Typ");

                entity.Property(e => e.TwrTypMinimum).HasColumnName("Twr_TypMinimum");

                entity.Property(e => e.TwrUdostepniajWcenniku).HasColumnName("Twr_UdostepniajWCenniku");

                entity.Property(e => e.TwrUpust).HasColumnName("Twr_Upust");

                entity.Property(e => e.TwrUpustData).HasColumnName("Twr_UpustData");

                entity.Property(e => e.TwrUpustDataDo).HasColumnName("Twr_UpustDataDo");

                entity.Property(e => e.TwrUpustDataOd).HasColumnName("Twr_UpustDataOd");

                entity.Property(e => e.TwrUpustGodz).HasColumnName("Twr_UpustGodz");

                entity.Property(e => e.TwrUpustGodzDo).HasColumnName("Twr_UpustGodzDo");

                entity.Property(e => e.TwrUpustGodzOd).HasColumnName("Twr_UpustGodzOd");

                entity.Property(e => e.TwrUrl)
                    .IsRequired()
                    .HasColumnName("Twr_URL")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.TwrWagaKg)
                    .HasColumnName("Twr_WagaKG")
                    .HasColumnType("decimal(15, 4)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TwrWaluta)
                    .IsRequired()
                    .HasColumnName("Twr_Waluta")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TwrZrodlowa)
                    .HasColumnName("Twr_Zrodlowa")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrZrodlowaExport)
                    .HasColumnName("Twr_ZrodlowaExport")
                    .HasColumnType("decimal(5, 2)");

                entity.Property(e => e.TwrZrodlowaZak)
                    .HasColumnName("Twr_ZrodlowaZak")
                    .HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<TwrCeny>(entity =>
            {
                entity.HasKey(e => e.TwCTwCid);

                entity.ToTable("TwrCeny", "CDN");

                entity.HasIndex(e => new { e.TwCTwrTyp, e.TwCTwrFirma, e.TwCTwrNumer, e.TwCTwrLp })
                    .HasName("TwCTwrGID");

                entity.HasIndex(e => new { e.TwCWartosc, e.TwCWaluta, e.TwCTwrId, e.TwCTwCnumer })
                    .HasName("TwCTowar")
                    .IsUnique();

                entity.Property(e => e.TwCTwCid).HasColumnName("TwC_TwCID");

                entity.Property(e => e.TwCAktualizacja).HasColumnName("TwC_Aktualizacja");

                entity.Property(e => e.TwCDokId).HasColumnName("TwC_DokID");

                entity.Property(e => e.TwCMarza)
                    .HasColumnName("TwC_Marza")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.TwCMarzaWstu)
                    .HasColumnName("TwC_MarzaWStu")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.TwCOffset)
                    .HasColumnName("TwC_Offset")
                    .HasColumnType("decimal(7, 4)");

                entity.Property(e => e.TwCPunkty).HasColumnName("TwC_Punkty");

                entity.Property(e => e.TwCTwCnumer).HasColumnName("TwC_TwCNumer");

                entity.Property(e => e.TwCTwrFirma).HasColumnName("TwC_TwrFirma");

                entity.Property(e => e.TwCTwrId).HasColumnName("TwC_TwrID");

                entity.Property(e => e.TwCTwrLp).HasColumnName("TwC_TwrLp");

                entity.Property(e => e.TwCTwrNumer).HasColumnName("TwC_TwrNumer");

                entity.Property(e => e.TwCTwrTyp).HasColumnName("TwC_TwrTyp");

                entity.Property(e => e.TwCTyp).HasColumnName("TwC_Typ");

                entity.Property(e => e.TwCWaluta)
                    .IsRequired()
                    .HasColumnName("TwC_Waluta")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TwCWartosc)
                    .HasColumnName("TwC_Wartosc")
                    .HasColumnType("decimal(17, 4)");

                entity.Property(e => e.TwCWartoscZakOld)
                    .HasColumnName("TwC_WartoscZakOld")
                    .HasColumnType("decimal(17, 4)");

                entity.Property(e => e.TwCZaokraglenie)
                    .HasColumnName("TwC_Zaokraglenie")
                    .HasColumnType("decimal(7, 4)");

                entity.HasOne(d => d.TwCTwr)
                    .WithMany(p => p.TwrCeny)
                    .HasForeignKey(d => d.TwCTwrId)
                    .HasConstraintName("FK_TwCTowar");
            });
        }
    }
}
