﻿using System.Collections.Generic;

namespace OptimaWebImport.Models
{
    public class BOMModel
    {
        public string Name { get; set; }
        public bool Complex { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
        public List<BOMModel> Children { get; set; }
        public string ParentName { get; set; }

    }
}