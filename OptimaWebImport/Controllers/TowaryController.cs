﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OptimaWebImport.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OptimaWebImport.Controllers
{
   
    public class TowaryController : Controller
    {
        TowaryDataAccessLayer objtowary = new TowaryDataAccessLayer();
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        [Route("api/BOM/Create")]
        public void Create([FromBody]JsonModel jsonmodel)
        {
            foreach (var item in jsonmodel.Boms)
            {
                objtowary.AddItem(item);
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
